package matrius;

import java.util.Scanner;

public class substituirMatriu {

	public static void main(String[] args) {
		
			Scanner sc = new Scanner(System.in);
			System.out.println("Escriu la mida de la matriu quadrada");
			int mida = sc.nextInt();
			sc.nextLine();
			
			char [][] mat = new char[mida][mida];
			
			for(int i=0; i<mida;i++) {
				for(int j=0; j<mida; j++) {
					mat[i][j]= sc.nextLine().charAt(0);
				}
			}
		System.out.println("Escriu quin caràcter busques");
		char buscat = sc.nextLine().charAt(0);
		System.out.println("Escriu per quin caràcter el vols substituir");
		char subs = sc.nextLine().charAt(0);
		
		for(int i=0; i<mida;i++) {
			for(int j=0; j<mida; j++) {
				if(mat[i][j]==buscat) {
					mat[i][j]= subs;
				}
			}
		}
		imprimeixMat(mat);
		
	}

	public static void imprimeixMat(char[][] mat) {
		for (int i = 0; i < mat.length; i++) {			
			for (int j = 0; j < mat[0].length; j++) {
				System.out.print(mat[i][j] + " ");
			}
			System.out.println();
		}
	}
}
