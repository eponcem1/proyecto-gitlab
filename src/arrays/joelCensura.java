package arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class joelCensura {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		List<String> paraules = new ArrayList<String>();

		for (int i = 0; i < casos; i++) {
			// Per cada cas llegim la llista de censura i després la frase.
			int linies = sc.nextInt();
			sc.nextLine();
			paraules.clear();
			for (int j = 0; j < linies - 1; j++) {
				paraules.add(sc.nextLine());
			}
			String frase = sc.nextLine();
			for (String paraula : paraules) {
				frase = frase.replaceAll(paraula, "*****");
			}
			System.out.println(frase);
		}
		sc.close();
	}
}
