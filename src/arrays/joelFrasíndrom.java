package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class joelFrasíndrom {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		ArrayList<String> frase = new ArrayList<String>();
		String linea = sc.nextLine();
		int acumulador;
		while (!linea.equals(".")) {
			acumulador = 0;
			String[] elements = linea.split(" ");
				
			for (int i = 0; i < elements.length; i++) {
				if (elements[i].equals(elements[elements.length -1 - i])) {
					acumulador++;
				}
			}
			if (acumulador == elements.length) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}
			frase.clear();
			linea = sc.nextLine();
		}
	}

}
