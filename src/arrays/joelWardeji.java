package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class joelWardeji {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		sc.nextLine();

		for (int f = 0; f < casos; f++) {
			String[] text;
			text = sc.nextLine().split(" ");
			ArrayList<String> l1 = new ArrayList<>(Arrays.asList(text));
			text = sc.nextLine().split(" ");
			ArrayList<String> l2 = new ArrayList<>(Arrays.asList(text));
			text = sc.nextLine().split(" ");
			ArrayList<String> l3 = new ArrayList<>(Arrays.asList(text));
			text = sc.nextLine().split(" ");
			ArrayList<String> l4 = new ArrayList<>(Arrays.asList(text));

			ArrayList<String> resul = (ArrayList<String>) l1.clone();

			resul.retainAll(l2);
			resul.retainAll(l3);
			resul.retainAll(l4);


			Collections.sort(resul);

			int i = 0;
			while (i < resul.size() - 1) {
				if (resul.get(i).equals(resul.get(i + 1))) {
					resul.remove(i);
				} else {
					i++;
				}
			}

			System.out.println(resul);
		}

		
	}

}
