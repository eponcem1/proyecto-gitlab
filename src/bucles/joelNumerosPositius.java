package bucles;

import java.util.Scanner;

public class joelNumerosPositius {

	public static void main(String[] args) {
		int voltes;
		int acumulador = 0;
		Scanner llegir = new Scanner(System.in);
		voltes = llegir.nextInt();

		for (int i = 0; i < voltes; i++) {
			
			int num = llegir.nextInt();
			if (num > 0) {
				acumulador = acumulador + 1;
			}
		}
		System.out.println(acumulador);
	}

}
