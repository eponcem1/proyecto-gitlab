package Buscamines;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;
import java.util.random.*;

import Buscamines.PlayerBM;
import Buscamines.Posicio;

public class Buscamines {
	// 0 -> casella lliure
	// 1 -> casella amb mina
	// 0-8 -> casella descoberta que mostra nºmines
	// 9 -> casella sense descobrir
	// ? -> casella amb mina descoberta (fi partida)

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		PlayerBM j1 = new PlayerBM();
		Posicio pos = new Posicio();
		int seleccio;
		boolean sortir = false;
		ArrayList<String>winner = new ArrayList<String>();
		
		while(!sortir) {
			System.out.println("1.- Mostrar Ajuda");
			System.out.println("2.- Opcions");
			System.out.println("3.- Jugar Partida");
			System.out.println("4.- Veure Llista de Guanyadors");
			System.out.println("0.- Sortir");
			seleccio = sc.nextInt();
			if(seleccio==1) {
				System.out.println("El juego consiste en despejar todas las casillas de una pantalla que no oculten una mina.\r\n"
						+ "\r\n"
						+ "Algunas casillas tienen un número, este número indica las minas que son en todas las casillas circundantes. Así, si una casilla tiene el número 3, significa que de las ocho casillas que hay alrededor (si no es en una esquina o borde) hay 3 con minas y 5 sin minas. Si se descubre una casilla sin número indica que ninguna de las casillas vecinas tiene mina y estas se descubren automáticamente.\r\n"
						+ "\r\n"
						+ "Si se descubre una casilla con una mina se pierde la partida.\r\n"
						+ "\r\n"
						+ "Se puede poner una marca en las casillas que el jugador piensa que hay minas para ayudar a descubrir la que están cerca.");
			}else
				if(seleccio==2) {
					sc.nextLine();
					j1 = opcions(sc);
				}else
					if(seleccio==3) {
				int [][]	taulerMina = inicialitzarMines(j1);
				int [][]	taulerCamp = inicialitzarCamp(j1);
				visualitzarCamp( taulerCamp, j1);
						winner = jugar(j1, pos, taulerMina, taulerCamp, sc, winner);
					
					}else
						if(seleccio==4) {
							System.out.println(winner);
						}else
							if(seleccio==0) {
								sortir=true;
							}else {
								System.out.println("Burro torna a provar!");
							}
		}
		
		
	}
	/**
	 * Aquesta funció omple les dades necessaries per iniciar el joc.
	 * @param sc Es necessari per obtenir informació.
	 * @return jug Les dades del jugador
	 */
	
	private static PlayerBM opcions(Scanner sc) {
		PlayerBM jug = new PlayerBM();
		boolean seguro = false;
		String r = "";
		while(!seguro) {
			System.out.println("Como te llamas?");
		jug.nom = sc.nextLine();
		System.out.println("Numero de filas:");
		jug.f = sc.nextInt();
		System.out.println("Numero de columnas:");
		jug.c = sc.nextInt();
		System.out.println("Numero de minas:");
		jug.nMines = sc.nextInt();
		sc.nextLine();
		System.out.println("La configuración es correcta?");
		r = sc.nextLine();
		if((r.equals("Si"))|| (r.equals("si"))) {
			seguro = true;
		}
		
		}
		jug.guanyador = false;
		jug.mina = false;
		
		return jug;
	}
	
	/**
	 * Ubica les mines al tauler sense repeticions.
 	 * @param j1 mostra la grandaria del tauler.
	 * @param taulerMina es el que canvia.
	 * @return tauler amb la ubicacio de les mines.
	 */
	
	private static int[][] inicialitzarMines(PlayerBM j1) {
		Random r = new Random();
			int fila;
			int col;
			int [][] tauler = new int[j1.f][j1.c];
		for(int i=0; i<j1.f; i++) {
			for(int j=0; j<j1.c; j++) {
				tauler[i][j]= 0;
			}
		}
		for(int l=0;l<j1.nMines;l++) {
			
			fila = r.nextInt(0, j1.f);
			col = r.nextInt(0, j1.c);
			while((tauler[fila][col]==10)) {
				fila = r.nextInt(0, j1.f);
				col = r.nextInt(0, j1.c);
			}
			tauler[fila][col] = 10;
		}
		
		return tauler;
	}
	
	/**
	 * Crea la capa visual del joc.
	 * @param j1 determina el tamany del tauler.
	 * @param taulerCamp El que es modifica.
	 * @return tauler apartat visual.
	 */
	
	private static int[][] inicialitzarCamp(PlayerBM j1) {
		int [][]tauler = new int[j1.f][j1.c];
		for(int i=0; i<j1.f; i++) {
			for(int j=0; j< j1.c; j++) {
				tauler[i][j]=9;
			}
		}
		
		return tauler;
	}
	
	/**
	 * Imprimeix taulerCamp.
	 * @param taulerCamp es el que s'ha d'imprimir.
	 * @param j1 la mida del tauler.
	 */
	
	private static void visualitzarCamp(int [][]taulerCamp, PlayerBM j1) {
		for(int i=0; i< j1.f; i++) {
			for(int j=0;j<j1.c;j++) {
				System.out.print(taulerCamp[i][j]+" ");
			}
			System.out.println();
		}
		
		
	}
	private static void visualitzarMina(int [][]taulerMina, PlayerBM j1) {
		for(int i=0; i< j1.f; i++) {
			for(int j=0;j<j1.c;j++) {
				System.out.print(taulerMina[i][j]+" ");
			}
			System.out.println();
		}
		
		
	}
	
	/**
	 * Es on experimentem la part jugable.
	 * @param j1 Es important saber qui juga.
	 * @param pos Determina on volem l'interacció amb el joc.
	 * @param taulerMina No varia, però el necessitem per a la comprovació de la jugada.
	 * @param taulerCamp Es el que varia en la nostra partida.
	 * @param sc Es per les coordenades.
	 * @param winner Es l'arraylist de guanyadors.
	 * @return 
	 */
	
	private static ArrayList jugar(PlayerBM j1, Posicio pos, int[][] taulerMina, int[][] taulerCamp, Scanner sc, ArrayList winner) {
		boolean partidaEnCurs = true;
		j1.mina = false;
		
		while(partidaEnCurs == true) {
			pos = demanarCoords(pos, j1, sc);
			taulerCamp = descobrir(pos.X, pos.Y,  taulerCamp, taulerMina, j1);
			partidaEnCurs = partidaAcabada(pos, j1, taulerCamp);
			visualitzarCamp(taulerCamp, j1);
		}
	winner= fiPartida(taulerCamp, taulerMina, winner, pos, j1);
	return winner;
	}
	
	/**
	 * Es demana les coordenades per saber on es fara l'acció.
	 * @param pos Es el que es modifica.
	 * @param sc Es un escanner, però els de Code Lyoko.
	 * @return posi La posició X e Y.
	 */
	
	private static Posicio demanarCoords(Posicio pos, PlayerBM j1, Scanner sc) {
		Posicio posi = new Posicio();
		
		
		System.out.println("En que columna vols mirar?");
		posi.X = sc.nextInt();
		while((posi.X<0)||(posi.X>j1.c)) {
			System.out.println("No em prenguis el pèl "+j1.nom+"! \n Torna a provar.");
			posi.X = sc.nextInt();
		}
		System.out.println("En que fila vols mirar?");
		posi.Y = sc.nextInt();
		while((posi.Y<0)||(posi.Y>j1.f)) {
			System.out.println("No em prenguis el pèl "+j1.nom+"! \n Torna a provar.");
			posi.Y = sc.nextInt();
		}
		
		return posi;
	}
	
	/**
	 * Aqui es modifica la zona de joc amb les dades del taulerMina.
	 * @param pos Ens diu on he de modificar la zona de joc.
	 * @param taulerCamp Es la zona de joc modificable.
	 * @param taulerMina Es la ubicació de les bombes.
	 * @param j1 Es la mida de la zona de joc.
	 * @return taulerCamp Es retorna ja modificat.
	 */
	
	private static int[][] descobrir( int X, int Y, int[][] taulerCamp, int[][] taulerMina, PlayerBM j1){

		
		
			
		int n = destapar(X, Y, taulerMina, j1);
		if(n!=0) {
		taulerCamp[Y][X]= n;
		}else
		if(n==0){
			taulerCamp[Y][X]= n;
			if(X !=0 && Y!=0 && taulerCamp[Y-1][X-1]==9) {
			descobrir(X-1, Y-1, taulerCamp, taulerMina, j1);
			}
			if(Y!=0 && taulerCamp[Y-1][X]==9) {
				descobrir(X, Y-1, taulerCamp, taulerMina, j1);
				}
			if(X !=j1.c-1 && Y!=0 && taulerCamp[Y-1][X+1]==9) {
				descobrir(X+1, Y-1, taulerCamp, taulerMina, j1);
				}
			if(X !=0 && taulerCamp[Y][X-1]==9) {
				descobrir(X-1, Y, taulerCamp, taulerMina, j1);
				}
			if(X !=j1.c-1 && taulerCamp[Y][X+1]==9) {
				descobrir(X+1, Y, taulerCamp, taulerMina, j1);
				}
			if(X !=0 && Y!=j1.f-1 && taulerCamp[Y+1][X-1]==9) {
				descobrir(X-1, Y+1, taulerCamp, taulerMina, j1);
				}
			if(Y!=j1.f-1 && taulerCamp[Y+1][X]==9) {
				descobrir(X, Y+1, taulerCamp, taulerMina, j1);
				}
			if(X !=j1.c-1 && Y!=j1.f-1 && taulerCamp[Y+1][X+1]==9) {
				descobrir(X+1, Y+1, taulerCamp, taulerMina, j1);
				}
			
	}
		return taulerCamp;
	}
	
	/**
	 * Aqui es comprova el número de mines al voltant d'una casella.
	 * @param pos Determina la casella que dira el número.
	 * @param taulerMina Determina si explotem o no.
	 * @param j1 La mida del tauler.
	 * @return comptador Es el número de mines al voltant de la casella.
	 */
	
	private static int destapar(int X, int Y, int[][] taulerMina, PlayerBM j1) {
		int comptador=0;
		
		if(taulerMina[Y][X] == 10) {
			j1.mina=true;
			comptador = 10;
		}else
			if(taulerMina[Y][X] == 0) {
				comptador=0;
				for(int i=0; i<j1.f; i++) {
					for(int j=0; j<j1.c; j++) {
						if(i>=Y-1 && i<=Y+1) {
							if(j>=X-1 && j<=X+1) {
								if(!(i == Y && j == X) && taulerMina[i][j] == 10) {
									comptador++;
								}
							}
						}
					}
				}
			}
		
		return comptador;
	}
	
	/**
	 * Diu si la partida a acabat o continua.
	 * @param pos Un boolean sobre si a tocat una mina.
	 * @param j1 Un boolean sobre si a guanyat o a perdut.
	 * @param taulerCamp La zona de joc a investigar.
	 * @return partidaEnCurs Si la partida segueix sera true.
	 */
	private static boolean partidaAcabada(Posicio pos, PlayerBM j1, int[][] taulerCamp) {
		boolean partidaEnCurs=true;
		int casellesLliures = CasellesLliures(taulerCamp, j1);
		
		if(j1.nMines == casellesLliures || j1.mina==true) {
			partidaEnCurs=false;
		}
		
		
		return partidaEnCurs;
	}
	
	/**
	 * Fa un recompte de les caselles sense descobrir.
	 * @param taulerCamp Es d'on treu el recompte.
	 * @param j1 Determina la grandaria de la zona de joc.
	 * @return contador El número de caselles sense descobrir.
	 */
	private static int CasellesLliures(int[][]taulerCamp, PlayerBM j1) {
		int contador =0;
		
		for(int i=0; i<j1.f; i++) {
			for(int j=0; j< j1.c; j++) {
				if(taulerCamp[i][j]==9) {
					contador++;
				}
			}
		}
		
		return contador;
	}
	
	private static ArrayList fiPartida(int[][] taulerCamp, int[][] taulerMina, ArrayList winner, Posicio pos, PlayerBM j1){
		int n = CasellesLliures(taulerCamp, j1);
		if(j1.mina == true) {
			System.out.println("Game Over"+ "\n"+ j1.nom+ " has perdut!");
		}else
			if(n == j1.nMines) {
				j1.guanyador=true;
				System.out.println(j1.nom+ " el teu nom quedara al registre de guanyadors");
				winner.add(j1.nom);
			}
		
		return winner;
	}
	
	
}
