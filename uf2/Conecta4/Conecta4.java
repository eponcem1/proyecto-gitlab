package Conecta4;

import java.util.Scanner;

import LaOca.Players;

public class Conecta4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		Player j1 = new Player();
		Player j2 = new Player();
		boolean sortir = false;
		char[][] tauler = inicialitzaTauler(sc);
		int seleccio=0;
		while(!sortir) {
			System.out.println("1- Instrucións");
			System.out.println("2- Configuració");
			System.out.println("3- Jugar");
			System.out.println("0- Sortida");
			seleccio = sc.nextInt();
			
			if(seleccio == 1) {
				System.out.println("El connecta 4 es un joc a on s’han de posar en linea 4 fitxes del mateix color."+ '\n' + "Per torns els jugadors introdueixen una fitxa en la columna que vulguin i aquesta caurà a la posició més baixa."+ '\n' + "Guanya la partida el primer en alinear 4 fitxes del mateix color." + '\n' + "Si totes les columnes queden plenes però ningú ha guanyat es dona empat.");
			}else
				if(seleccio == 2) {
					sc.nextLine();
					j1 = omplirJugador(sc);
					j2 = omplirJugador(sc);
					tauler = inicialitzaTauler(sc);
				}else
					if(seleccio == 3) {
						Jugar(j1, sc, tauler);
						Jugar(j2, sc, tauler);
					}else
						if(seleccio == 0) {
							sortir = true;
						}else {
							System.out.println("IDIOTA!");
							System.out.println("Escriu un número dins les opcions");
						}
		}
		
	}
	
	private static char[][] inicialitzaTauler(Scanner sc) {
		char[][] tauler = new char[4][4];
		for(int i=0; i<4;i++) {
			for(int j=0; j<4; j++) {
				tauler[i][j]= '▢';
			}
		}
		return tauler;
	}
	private static Player omplirJugador(Scanner sc) {
		Player jug = new Player();
		System.out.println("Nom del jugador: ");
		jug.nom = sc.nextLine();
		System.out.println("Color del jugador: Vermell(V), Blau(B) o Groc(G)");
		jug.color = sc.nextLine().charAt(0);
		jug.guanyador= false;
		jug.casella=0;
		return jug;
		
	}
	private static void veureTauler(char [][]tauler){
		for(int i=0; i<4;i++) {
			for(int j=0; j<4; j++) {
				System.out.print(" "+tauler[i][j]);
			}
			System.out.println();
		}
	}
	private static void Jugar(Player jugador, Scanner sc, char [][]tauler) {
		tauler = Tirada(jugador, sc, tauler);
		veureTauler(tauler);
		
	}
	private static char[][] Tirada(Player jugador, Scanner sc, char[][]tauler) {
		System.out.println(jugador.nom + " columna on fer tirada:");
		jugador.casella = sc.nextInt();
		while(!(tauler[0][jugador.casella] == '▢')) {
			System.out.println(jugador.nom + " columna on fer tirada:");
			jugador.casella =sc.nextInt();
		}
		for(int i=0; i<3; i++) {
			
				if(tauler[i+1][jugador.casella] == '▢') {
					tauler[i+1][jugador.casella] = jugador.color;
					tauler[i][jugador.casella] = '▢';
				}else 
					if((tauler[i+1][jugador.casella] != '▢')&&(tauler[i][jugador.casella]=='▢')){
						tauler[i][jugador.casella]=jugador.color;
					}
					
				
			
		}
		return tauler;
	}
}
