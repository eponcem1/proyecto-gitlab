package JocDeLaVida;

import java.util.Random;
import java.util.Scanner;

public class jocDeLaVida {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		boolean sortir = false;
		int[][] tauler = inicialitzarTauler(sc);
		while (!sortir) {
			opcions();
			int opció = sc.nextInt();
			if (opció == 1) {
				tauler = inicialitzarTauler(sc);
			} else if (opció == 2) {
				visualitzarTauler(tauler);
			} else if (opció == 3) {
				iterar(tauler, sc);

			} else if (opció == 0) {
				sortir = true;
			} else {
				System.out.println("Try again");
			}
		}
	}

	public static void opcions() {
		System.out.println("Tria una opció:");
			System.out.println("1.-	Inicialitzar tauler");
			System.out.println("2.-	Visualitzar tauler");
			System.out.println("3.-	Iterar");
			System.out.println("0.-	Sortir");
	}
	
	public static int[][] inicialitzarTauler(Scanner sc) {
		System.out.println("Y=...");
		int y = sc.nextInt();
		System.out.println("X=...");
		int x = sc.nextInt();
		while (!(y >= 4 && y <= 10) && !(x >= 4 && x <= 10)) {
			System.out.println("Torna a provar amb Y=...");
			y = sc.nextInt();
			System.out.println("Torna a provar amb X=...");
			x = sc.nextInt();
		}

		int[][] tauler = new int[y][x];
		omplirMatriuInts(tauler);

		Random r = new Random();
		int ry = 0;
		int rx = 0;
		for (int i = 0; i < ((y * x) / 2); i++) {
			
			ry = r.nextInt(0, y);
			rx = r.nextInt(0, x);
			while(tauler[ry][rx]==1) {
				ry = r.nextInt(0, y);
				rx = r.nextInt(0, x);
			}
			tauler[ry][rx] = 1;
		}

		return tauler;
	}

	public static void omplirMatriuInts(int[][] tauler) {

		for (int i = 0; i < tauler.length; i++) {
			for (int j = 0; j < tauler[0].length; j++) {
				tauler[i][j] = 0;
			}
		}

	}

	public static void visualitzarTauler(int[][] tauler) {
		for (int i = 0; i < tauler.length; i++) {
			for (int j = 0; j < tauler[0].length; j++) {

				System.out.print(tauler[i][j] + " ");

			}
			System.out.println();
		}
	}

	public static int[][] iterar(int[][] tauler, Scanner sc) {
		System.out.println("Tauler actual:");
		visualitzarTauler(tauler);

		System.out.println("Quantes iteracións vols fer?");
		int it = sc.nextInt();		
		while (!(it > 0 && it < 101)) {
			System.out.println("Quantes iteracións vols fer?");
			it = sc.nextInt();
		}
		int[][] tauler2 = new int[tauler.length][tauler.length];
		omplirMatriuInts(tauler2);
		int recompte = 0;
		for (int itera = 0; itera < it; itera++) {
			for (int i = 0; i < tauler.length; i++) {
				for (int j = 0; j < tauler[0].length; j++) {
					recompte = 0;
					if (tauler[i][j] == 1) {
//						for (int y = 0; y < tauler.length; y++) {
//							for (int x = 0; x < tauler[0].length; x++) {
//								if (y >= i - 1 && y <= i + 1) {
//									if (x >= j - 1 && x <= j + 1) {
//										if (!(y == i && x == j) && tauler[y][x] == 1) {
//											recompte++;
//										}
//									}
//								}
//							}
//						}
						recompte = comprobarVeins(tauler, i, j);
						if (recompte == 2 || recompte == 3) {
							tauler2[i][j] = 1;
						} else {
							tauler2[i][j] = 0;
						}
					} else if (tauler[i][j] == 0) {
//						for (int y = 0; y < tauler.length; y++) {
//							for (int x = 0; x < tauler[0].length; x++) {
//								if (y >= i - 1 && y <= i + 1) {
//									if (x >= j - 1 && x <= j + 1) {
//										if (!(y == i && x == j) && tauler[y][x] == 1) {
//											recompte++;
//										}
//									}
//								}
//							}
//						}
						recompte = comprobarVeins(tauler, i, j);
						if (recompte != 3) {
							tauler2[i][j] = 0;
						} else {
							tauler2[i][j] = 1;
						}
					}
				}
			}
			tauler = evoluciona(tauler, tauler2);
			visualitzarTauler(tauler);
			System.out.println();
		}

		return tauler;
	}

	public static int[][] evoluciona(int[][] tauler, int[][] tauler2) {
		for (int a = 0; a < tauler.length; a++) {
			for (int b = 0; b < tauler[0].length; b++) {
				tauler[a][b] = tauler2[a][b];
			}
		}
		return tauler;
	}
	
	public static int comprobarVeins(int[][] tauler, int i, int j) {
		int recompte =0;
		for (int y = 0; y < tauler.length; y++) {
			for (int x = 0; x < tauler[0].length; x++) {
				if (y >= i - 1 && y <= i + 1) {
					if (x >= j - 1 && x <= j + 1) {
						if (!(y == i && x == j) && tauler[y][x] == 1) {
							recompte++;
						}
					}
				}
			}
		}
		return recompte;
	}
}
