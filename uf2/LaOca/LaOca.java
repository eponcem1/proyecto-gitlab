package LaOca;

import java.io.Console;
import java.util.Scanner;
import java.util.ArrayList;

public class LaOca {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Players j1 = new Players();
		Players j2 = new Players();
		boolean sortir = false;
		int []tauler;
		tauler = new int [64];
		omplirArray(tauler);
		
		while(!sortir) {
			System.out.println("1- Inicialitzar joc");
			System.out.println("2- Visualitzar tauler");
			System.out.println("3- Jugar");
			System.out.println("0- Sortir");
			int seleccion= sc.nextInt();
			sc.nextLine();
			if(seleccion == 1) {
				j1 = InicialitzarJoc(sc);
				j2 = InicialitzarJoc(sc);
			}
			else if(seleccion == 2) {
				veureTauler(tauler, j1, j2);
			}
			else if(seleccion == 3) {
				int casella1 =Jugar(sc, j1);
				veureTauler(tauler, j1, j2);
				if(j1.casella == 63) {
					sortir = true;
				}
				int casella2 =Jugar(sc, j2);
				veureTauler(tauler, j1, j2);
				if(j2.casella == 63) {
					sortir = true;
				}
			}
			else if(seleccion == 0) {
				sortir = true;
			}
			else {
				System.out.println("Burro!"+ "\n" + "Prova un altre cop:");
			}
		}
		

	}
	private static Players InicialitzarJoc(Scanner sc) {
		Players jug = new Players();
		System.out.println("Nom del jugador:");
		jug.nom = sc.nextLine();
		jug.guanyador = false;
		jug.casella = 1;
		return jug;
	}
	private static void omplirArray(int []tauler) {
		for(int i=0; i<tauler.length; i++) {
			tauler[i]= i;
		}
	}
	private static int[] veureTauler(int []tauler, Players j1, Players j2) {
		for(int i=1; i<tauler.length; i++) {
			if(j1.casella == i && j2.casella == i) {
				System.out.print("["+j1.nom + " "+ "i "+j2.nom+"]");
			}else {
			if(j1.casella == i) {
				System.out.print("["+j1.nom+"]");
			}else if(j2.casella == i) {
				System.out.print("["+j2.nom+"]");
			}
			else{
			System.out.print("[" +tauler[i]+ "]");
			}
			if((i == 16)||(i==32)||(i==48)||(i==63)) {
				System.out.println();
			}
		}
		
	}return tauler;
	}
	private static int Jugar(Scanner sc, Players player){
		boolean tirada = false;
		int dau =0;
		int diferencia=0;
		int casella = player.casella;
		System.out.println(player.nom);
		while(!tirada) {
			dau =DauUnoSeis(dau, sc);
			System.out.println(dau);
			if(player.casella + dau > 63) {
				diferencia = (player.casella + dau) -63;
				player.casella = 63-diferencia;
			}else {
			player.casella = dau + player.casella;
			}
			tirada = calsella(player, tirada);
		}
		
//		for(int b=0;b<100;b++) {
//		System.out.println();
//		}
		return casella;
	}
	private static int DauUnoSeis(int dau, Scanner sc) {
		dau=0;
		while(!((dau<=6)&&(dau>=1))){
			System.out.println("Tira el dau:");
			dau = sc.nextInt();
		}
		return dau;
	}
	
	private static boolean calsella(Players player, boolean tirada) {
		if(player.casella == 5|| player.casella == 9|| player.casella == 14|| player.casella == 18|| player.casella == 23|| player.casella == 27|| player.casella == 32|| player.casella == 36|| player.casella == 41|| player.casella == 45|| player.casella == 50|| player.casella == 54|| player.casella == 59) {
			casOca(player);
			System.out.println("D'oca a oca i tiro perquè em toca");
		}else
			if(player.casella == 26|| player.casella == 53) {
				DauADau(player);
				System.out.println("De dau a dau i tiro perquè m'ha tocat");
			}else
				if(player.casella == 42) {
					player.casella = 30;
					System.out.println("Perdut al laberint. Torna a la casella 30.");
					tirada = true;
				}else
					if(player.casella == 58) {
						player.casella = 0;
						System.out.println("Has mort, torna a la casella d'inici.");
						tirada = true;
					}else
						if(player.casella == 63) {
							System.out.println("Enorabona!!!");
							System.out.println("Ha guanyat: "+ player.nom);
							tirada = true;
						}else {
							tirada = true;
						}
		
		return tirada;
	}
	
	private static void casOca(Players player){
		if(player.casella==5) {
			player.casella = 9;
		}else
			if(player.casella==9) {
				player.casella = 14;
			}else
				if(player.casella==14) {
					player.casella = 18;
				}else
					if(player.casella==18) {
						player.casella = 23;
					}else
						if(player.casella==23) {
							player.casella = 27;
						}else
							if(player.casella==27) {
								player.casella = 32;
							}else
								if(player.casella==32) {
									player.casella = 36;
								}else
									if(player.casella==36) {
										player.casella = 41;
									}else
										if(player.casella==41) {
											player.casella = 45;
										}else
											if(player.casella==45) {
												player.casella = 50;
											}else
												if(player.casella==50) {
													player.casella = 54;
												}else
													if(player.casella==54) {
														player.casella = 59;
													}else
														if(player.casella==59) {
															player.casella = 63;
														}
	}
	private static void DauADau(Players player) {
		if(player.casella == 26) {
			player.casella = 53;
		}else
			if(player.casella == 53) {
				player.casella = 26;
			}
	}
}
