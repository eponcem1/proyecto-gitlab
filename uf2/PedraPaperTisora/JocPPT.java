package PedraPaperTisora;


import java.io.Console;
import java.util.Scanner;


public class JocPPT {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
//		j1.nom = "Manu";
//		j1.guanyador = false;
//		j2.nom = "Ethan";
//		j2.guanyador = false;
		
		// CREAR JUGADORS
		// Inclou demanar dades
		
		JugadorPPT j1 = new JugadorPPT();
		JugadorPPT j2 = new JugadorPPT();
		
		j1 = omplirJugador(sc);
		j2 = omplirJugador(sc);
		
		// Preguntar num tirades O bé, fixar-lo nosaltres
		
		int tirades = 3;
		
		// BUCLE TIRADES
			// RESOLDRE 1 TIRADA
				// RECOLLIR TIRADA j1   Rock Paper Scissors
				// RECOLLIR TIRADA j2
				// GESTIONAR GUANYADOR TIRADA
					// FER COMPARATIVA
					// ACTUALITZAR COMPTADORS (inclou print)

		boolean sortir = false;
		int guanyapunt = 0;
//		j1.punts =0;
//		j2.punts =0;
		while (!sortir && j1.punts < tirades && j2.punts < tirades) {
			// ANEM FENT TIRADES
			// i CAL VEURE SI LA PARTIDA ACABA
			char tirada1 = recollirTirada(sc, j1);
			char tirada2 = recollirTirada(sc, j2);
			
//			comprovarResultat()
			
//			mostrarResultat()
			
//			if(tirada1 == tirada2) {
//				// EMPAT NO FEM RES
//				guanyapunt =0;
//			} else {
//				if(tirada1 == 'R') {
//					if(tirada2 == 'P') {
//						j2.punts++;
//						guanyapunt = 2;
//					} else {
//						// 'S'
//						j1.punts++;
//						guanyapunt = 1;
//					}
//				} else if(tirada1 == 'P') {
//					// casos paper
//					if(tirada2 == 'R') {
//						j1.punts++;
//						guanyapunt = 1;
//					} else {
//						// 'S'
//						j2.punts++;
//						guanyapunt = 2;
//					}					
//				} else {
//					// Casos tisora
//					if(tirada2 == 'R') {
//						j2.punts++;
//						guanyapunt = 2;
//					} else {
//						// 'P'
//						j1.punts++;
//						guanyapunt = 1;
//					}
//				}
//			}
			
			guanyapunt = comprovarResultat( tirada1, tirada2, guanyapunt, j1, j2);
			mostrarResultat(j1, j2, guanyapunt);
//			if(guanyapunt != 0) {
//				System.out.println("Punt aconseguit al jugador " + guanyapunt);
//			} else {
//				System.out.println("Punt empatat");
//			}
//			System.out.println(j1.nom+ " tens " + j1.punts + " punts");
//			System.out.println(j2.nom+ " tens " + j2.punts + " punts");
			
			
		}
		
		// PROCLAMAR GUANYADOR
		if(j1.punts > j2.punts) {
			System.out.println(j1.nom + " has guanyat!!");
		}
		
		if(j1.punts < j2.punts) {
			System.out.println(j2.nom +" has guanyat!!");
		}
		
	}
	
	private static char recollirTirada(Scanner sc, JugadorPPT jugador) {
		System.out.println("Hola "+jugador.nom+ " si us plau, escull una opció:");
		System.out.println("(R)ock (P)aper (S)cissors");
		char lletra = sc.nextLine().charAt(0);
		while (lletra != 'R' &&lletra != 'P' &&lletra != 'S' ) {
			System.out.println("Hola "+jugador.nom+ " si us plau, escull una opció:");
			System.out.println("(R)ock (P)aper (S)cissors");
			lletra = sc.nextLine().charAt(0);		
		}
		return lletra;
	}

	private static JugadorPPT omplirJugador(Scanner sc) {
		JugadorPPT jug = new JugadorPPT();
		// faré coses com els catalans
		jug.nom = sc.nextLine();
		jug.guanyador = false;
		jug.punts = 0;
		return jug;
	}
	private static int comprovarResultat( char tirada1, char tirada2, int guanyapunt, JugadorPPT j1, JugadorPPT j2) {
		if(tirada1 == tirada2) {
			// EMPAT NO FEM RES
			guanyapunt = 0;}
			else {
				if(tirada1 == 'R') { 
					if (tirada2 == 'P') {
					j2.punts++;
					guanyapunt = 2;
					}
				else {j1.punts++;
				guanyapunt = 1;
				}
			
				} else 
					if(tirada1 == 'P') {
					// casos paper
					if(tirada2 == 'R') {
						j1.punts++;
						guanyapunt = 1;
					} else {
						// 'S'
						j2.punts++;
						guanyapunt = 2;
					}					
				}
			else {
			// Casos tisora
			if(tirada2 == 'R') {
				j2.punts++;
				guanyapunt = 2;
			} else {
				// 'P'
				j1.punts++;
				guanyapunt = 1;
			}
		}
}
		return guanyapunt;
		}
	private static void mostrarResultat(JugadorPPT j1, JugadorPPT j2, int guanyapunt) {
		if(guanyapunt != 0) {
			System.out.println("Punt aconseguit al jugador " + guanyapunt);
		} else {
			System.out.println("Punt empatat");
		}
		System.out.println(j1.nom+ " tens " + j1.punts + " punts");
		System.out.println(j2.nom+ " tens " + j2.punts + " punts");
		
	}
}